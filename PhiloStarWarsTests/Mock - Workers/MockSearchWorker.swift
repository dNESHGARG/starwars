//
//  MockSearchWorker.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
@testable import PhiloStarWars

extension MockSearchWorker: Mockable {
    static func mock() -> Self {
        return MockSearchWorker(session: URLSession.shared) as! Self
    }
}

class MockSearchWorker: Searchable {
    var service: Service
    var urlBeingExecuted: URL?
    init(session: URLSession) {
        self.service = SearchService(session: session)
    }
}

extension MockSearchWorker {
    func search(with searchString: String,
                endpoint: Endpoint,
                completion: @escaping(Result<SearchResponse>) -> Void) {
        let lowercased = searchString.lowercased()
        let request = service.makeRequest(for : service,
                                          endpoint: endpoint,
                                          serviceType: .search,
                                          item: lowercased)!
        self.urlBeingExecuted = request.url
        return execute(request, service.session) { (result: Result<SearchResponse>) in
            switch result {
            case let .success(model):
                completion(Result.success(model))
            case let .failure(error):
                completion(Result.failure(error))
            }
        }
    }
}


extension MockSearchWorker {
    enum ModelType {
        case test, mock
    }
    
    func execute<T>(_ request: URLRequest, _ session: URLSession, _ decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T>) -> Void) where T : Decodable {
        
        let url = request.url
        let mock = getAllModelMock(url)

        guard let _ = mock else {
            completion(Result.failure(.noResponse))
            return
        }
        
        let modeltype = ModelType.test
        
        let model: Any
        switch modeltype {
        case .test: model = SearchResponse.mock()
        case .mock: model = SearchResponse.mock()
        }
        
        if let value = model as? T {
            completion(.success(value))
        } else  {
            (completion(Result.failure(.invalidData)))
        }
    }
}
