//
//  MockWorkerContainer.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
@testable import PhiloStarWars

struct MockWorkersContainer {
    let searchWorker: MockSearchWorker
    let allWorker: LoadAllWorker
    let detailsWorker: MockDetailsWorker
    let imageWorker: SwapImageWorker
}
