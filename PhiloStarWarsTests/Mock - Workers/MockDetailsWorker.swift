//
//  MockDetailsWorker.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
@testable import PhiloStarWars

extension MockDetailsWorker: Mockable {
    static func mock() -> Self {
        return MockDetailsWorker(session: URLSession.shared) as! Self
    }
}

class MockDetailsWorker: Detailable {
    var service: Service
    var urlBeingExecuted: URL?
    init(session: URLSession) {
        self.service = DetailsService(session: session)
    }
}

extension MockDetailsWorker {
    func details(with searchString: String, endpoint: Endpoint, completion: @escaping(Result<AllDetailsModel>) -> Void)  {
        let request = service.makeRequest(for : service,
                                          endpoint: endpoint,
                                          serviceType: .detail,
                                          item: searchString)!
        self.urlBeingExecuted = request.url
        return execute(request, service.session) { (result: Result<AllDetailsModel>) in
                 switch result {
                 case let .success(model):
                     completion(Result.success(model))
                 case let .failure(error):
                     completion(Result.failure(error))
                 }
             }
    }
}


extension MockDetailsWorker {
    enum ModelType {
        case test, mock
    }
    
    func execute<T>(_ request: URLRequest, _ session: URLSession, _ decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T>) -> Void) where T : Decodable {
        
        let url = request.url
        let mock = getDetailsMock(url)

        guard let _ = mock else {
            completion(Result.failure(.noResponse))
            return
        }
        
        let modeltype = ModelType.test
        
        let model: Any
        switch modeltype {
        case .test: model = AllDetailsModel.mock()
        case .mock: model = AllDetailsModel.mock()
        }
        
        if let value = model as? T {
            completion(.success(value))
        } else  {
            (completion(Result.failure(.invalidData)))
        }
    }
}
