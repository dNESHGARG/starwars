//
//  DetailableDirectorTests.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import XCTest
@testable import PhiloStarWars

class DetailableDirectorTests: XCTestCase {
    var module: MockAppEnvironment!
    var director: DetailableDirector!
    
    override func setUp() {
        self.module = MockAppEnvironment.mock()
        self.director = module.directors.detailsDirector
    }
    
    func testDetailsDirectorLoadedData() {
        let val = "1"
        director.details(with: val, endpoint: .people) { model in
            switch model {
            case let .loaded(value):
                XCTAssertTrue(true, "Expected result to be loaded. Found value \(value)")
            case .notRequested:
                assertionFailure("State is expected to be .loaded, found .notRequested")
            case let .failed(error):
                assertionFailure("State is expected to be .loaded, found .failed(error) -\(error)")
            case .loading:
                assertionFailure("State is expected to be .loaded, found .loading(prev)")
            }
        }
    }
    
    func testDetailsDirectorFailedToLoadData() {
        let val = "3s6"
        director.details(with: val, endpoint: .people) { model in
            switch model {
            case let .loaded(value):
                assertionFailure("Expected result to be .failed. Found value \(value)")
            case .notRequested:
                assertionFailure("State is expected to be .failed(error), found .notRequested")
            case let .failed(error):
                XCTAssertTrue(true, "State is expected to be .failed(error), found .failed - \(error)")
            case .loading:
                assertionFailure("State is expected to be .failed(error), found .loading(prev)")
            }
        }
    }
    
    func testDetailsDirectorEndpoint() {
        let url = URL(string: "https://swapi.dev/api/people/1")
        let val = "1"
        let worker = self.director.worker as? MockDetailsWorker
        director.details(with: val, endpoint: .people) { model in
            XCTAssertEqual(url, worker?.urlBeingExecuted)
        }
    }
    
    func testDetailsDirectorFailedEndpoint() {
        let url = URL(string: "https://swapi.dev/api/people/1")
        let val = "1"
        let worker = self.director.worker as? MockDetailsWorker
        director.details(with: val, endpoint: .film) { model in
            XCTAssertNotEqual(url, worker?.urlBeingExecuted, "url being executed is for incorrect endpoint.")
        }
    }

}
