//
//  SearchableDirectorTests.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import XCTest
@testable import PhiloStarWars

class SearchableDirectorTests: XCTestCase {
    var module: MockAppEnvironment!
    var director: SearchableDirector!
    
    override func setUp() {
        self.module = MockAppEnvironment.mock()
        self.director = module.directors.searchDirector
    }
    
    
    func testDirectorFailedToInitializeState() {
        
    }
    
    func testSearchDirectorFailedToLoadData() {
        director.search(with: "Ver", endpoint: .people)
        let state = module.appState
        switch state.searchResults {
        case let .loaded(value):
            assertionFailure("expected no results found \(value.count) results")
        case .notRequested:
            assertionFailure("State is expected to be .failed(error), found .notRequested")
        case let .failed(error):
            XCTAssertTrue(true, "State is expected to be .failed(error), found .failed - \(error)")
        case .loading:
            assertionFailure("State is expected to be .failed(error), found .loading(prev)")
        }
    }
    
    func testSearchDirectorLoadData() {
        self.director.search(with: "vader", endpoint: .people)
        let state = module.appState
        switch state.searchResults {
        case let .loaded(value):
            XCTAssertTrue(state.searchResults == .loaded(value), "Expected Search Results to be loaded")
        default:
            assertionFailure("State is expected to be .loaded")
        }
    }
    
    func testSearchDirectorEndpoint() {
        let url = URL(string: "https://swapi.dev/api/people/?search=vader")
        let worker = self.director.worker as? MockSearchWorker
        self.director.search(with: "Vader", endpoint: .people)
        XCTAssertEqual(url, worker?.urlBeingExecuted)
    }
    
    func testSearchDirectorEndpointCaseInsensitive() {
            self.director.search(with: "VaDer", endpoint: .people)
              let state = module.appState
              switch state.searchResults {
              case let .loaded(value):
                  XCTAssertTrue(state.searchResults == .loaded(value), "Expected Search Results to be loaded")
              default:
                  assertionFailure("State is expected to be .loaded for case insensitivity")
              }
     }
    
    func testSearchDirectorFailedEndpoint() {
        let url = URL(string: "https://swapi.dev/api/people/?search=vader")
        let worker = self.director.worker as? MockSearchWorker
        self.director.search(with: "Vader", endpoint: .film)
        XCTAssertNotEqual(url, worker?.urlBeingExecuted, "url being executed is for incorrect endpoint.")
    }
}
