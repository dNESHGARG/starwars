//
//  Helpers.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
@testable import PhiloStarWars

func prepareAllModelMock() -> [URL?: SearchResponse] {
    let url = URL(string: "https://swapi.dev/api/people/?search=vader")
    var mocks = [URL?: SearchResponse]()
    mocks[url] = SearchResponse.mock()
    return mocks
}

func getAllModelMock(_ url: URL?) -> SearchResponse? {
    let mock = prepareAllModelMock()
    let key = mock.keys
    var model: SearchResponse?
    for k in key {
        if (k == url) {
            model =  mock[k]
        }
    }
    
    return model
}

func prepareDetailsMock() -> [URL?: AllDetailsModel] {
    let url = URL(string: "https://swapi.dev/api/people/1")
    var mocks = [URL?: AllDetailsModel]()
    mocks[url] = AllDetailsModel.mock()
    return mocks
}

func getDetailsMock(_ url: URL?) -> AllDetailsModel? {
    let mock = prepareDetailsMock()
    let key = mock.keys
    var model: AllDetailsModel?
    for k in key {
        if (k == url) {
            model =  mock[k]
        }
    }
    
    return model
}

func archive<T: Decodable>(data: T?) -> Data {
    var decode = data
    return Data(bytes: &decode, count: MemoryLayout<T>.stride)
}
