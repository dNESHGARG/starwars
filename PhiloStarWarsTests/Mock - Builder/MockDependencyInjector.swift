//
//  MockDependencyInjector.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
@testable import PhiloStarWars

struct MockDependencyInjector: ViewModifier {
    
    let appState: AppState
    let directors: MockDirectorContainer
    
    init(appState: AppState, directors: MockDirectorContainer) {
        self.appState = appState
        self.directors = directors
    }
    
    func body(content: Content) -> some View {
        content
            .environment(\.directors, directors)
            .environmentObject(appState)
    }
}
