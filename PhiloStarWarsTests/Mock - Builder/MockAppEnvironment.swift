//
//  MockAppEnvironment.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
@testable import PhiloStarWars

struct MockAppEnvironment {
    let appState: AppState
    let directors: MockDirectorContainer
    let injectors: MockDependencyInjector
}

extension MockAppEnvironment: Mockable {
    /*
     Bootstrap all dependencies and inject for further usage.
     */
    public static func mock() -> MockAppEnvironment {
        let appState = AppState()
        let session =  mockConfiguredURLSession()
        let workers = mockBuildWorkers(session: session)
        let directors = mockBuildDirectors(appState: appState, workers: workers)
        
        let dependencyInjector = mockBuildDependencies(appState: appState, directors: directors)
        
        return MockAppEnvironment(appState: appState, directors: directors, injectors: dependencyInjector)
    }
    
    private static func mockConfiguredURLSession() -> URLSession {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        
        // and create the URLSession from that
        return URLSession(configuration: config)
    }
    
    private static func mockBuildWorkers(session: URLSession) -> MockWorkersContainer {
        
        // Repository for search media
        let searchWorker =  MockSearchWorker(session: session)
        let allWorker = LoadAllWorker(session: session)
        let detailsWorker = MockDetailsWorker(session: session)
        // Interactor to fetch images
        let imageWorker = SwapImageWorker(session: session)
        
        return MockWorkersContainer(searchWorker: searchWorker,
                                    allWorker: allWorker,
                                    detailsWorker: detailsWorker,
                                    imageWorker: imageWorker)
    }
    
    private static func mockBuildDirectors(appState: AppState, workers: MockWorkersContainer) -> MockDirectorContainer {
        // Director to search media
        let searchDirector = MockSearchDirector(worker: workers.searchWorker, state: appState)
        let loadAllDirector = LoadAllDirector(worker: workers.allWorker, state: appState)
        let detailsDirector = MockDetailsDirector(worker: workers.detailsWorker, state: appState)
        // Interactor to fetch images
        let imageDirector = SwapImageDirector(worker: workers.imageWorker, state: appState)
        
        return MockDirectorContainer(searchDirector: searchDirector,
                                     loadAllDirector: loadAllDirector,
                                     detailsDirector: detailsDirector,
                                     imageDirector: imageDirector)
    }
    
    static func mockBuildDependencies(appState: AppState, directors: MockDirectorContainer) -> MockDependencyInjector {
        
        // A view modifier with EnvironmentObject - AppState and Environment - Interactors.
        return MockDependencyInjector(appState: appState, directors: directors)
    }
    
}
