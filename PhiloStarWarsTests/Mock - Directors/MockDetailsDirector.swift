//
//  MockDetailsDirector.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
@testable import PhiloStarWars


struct MockDetailsDirector: DetailableDirector {
    var worker: Detailable
    var state: AppState
}

extension MockDetailsDirector {
    func details(with string: String,
                 endpoint: Endpoint,
                 completion: @escaping (LoadedView<AllDetailsModel>) -> Void) {
        
        return worker.details(with: string, endpoint: endpoint) { model in
            switch model {
            case let .success(value):
                completion(.loaded(value))
            case let .failure(error):
                completion(.failed(error))
            }
        }
    }
}

extension MockDetailsDirector: Mockable {
    static func mock() -> MockDetailsDirector {
        return MockDetailsDirector(worker: MockDetailsWorker.mock(), state: AppState())
    }
}
