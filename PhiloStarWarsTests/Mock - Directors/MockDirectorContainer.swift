//
//  MockDirectorContainer.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
@testable import PhiloStarWars

struct MockDirectorContainer: EnvironmentKey {
    
    /// Define all interactors here.
    /// Ideally we only need Search interactor, but we can easily expand the app
    /// to show all the resoources and this is how we will expand the interactors.
    /// This class is the single source of truth for all kind of interactors for
    /// different feature sets.
    let searchDirector: MockSearchDirector
    let loadAllDirector: LoadAllDirector
    let detailsDirector: MockDetailsDirector
    let imageDirector: SwapImageDirector
    
    init(searchDirector: MockSearchDirector,
         loadAllDirector: LoadAllDirector,
         detailsDirector: MockDetailsDirector,
         imageDirector: SwapImageDirector) {
        self.searchDirector = searchDirector
        self.loadAllDirector = loadAllDirector
        self.detailsDirector = detailsDirector
        self.imageDirector = imageDirector
    }
    
    static var defaultValue: MockDirectorContainer {
        return MockDirectorContainer.mock()
    }
}

extension EnvironmentValues {
    var directors: MockDirectorContainer {
        get { self[MockDirectorContainer.self] }
        set { self[MockDirectorContainer.self] = newValue }
    }
}

extension MockDirectorContainer: Mockable {
    static func mock() -> MockDirectorContainer {
        return MockDirectorContainer(searchDirector: MockSearchDirector.mock(),
                                     loadAllDirector: LoadAllDirector.mock(),
                                     detailsDirector: MockDetailsDirector.mock(),
                                     imageDirector: SwapImageDirector.mock())
    }
}
