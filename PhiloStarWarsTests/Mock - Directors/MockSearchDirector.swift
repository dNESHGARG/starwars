//
//  MockSearchDirector.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
@testable import PhiloStarWars

struct MockSearchDirector: SearchableDirector {
    var worker: Searchable
    var state: AppState
    
    var historyManager: HistoryManager = HistoryManager()
}

extension MockSearchDirector {
    func search(with string: String, endpoint: Endpoint) {
        worker.search(with: string,
                      endpoint: endpoint) { model in
                        weak var weakAppState = self.state
                        switch model {
                        case let .success(value):
                            weakAppState?.searchResults = .loaded(value)
                        case let .failure(error):
                            weakAppState?.searchResults = .failed(error)
                        }
        }
    }
}

extension MockSearchDirector: Mockable {
    static func mock() -> MockSearchDirector {
        return MockSearchDirector(worker: MockSearchWorker.mock(), state: AppState())
    }
}
