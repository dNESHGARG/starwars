//
//  Result+Extension.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/19/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
@testable import PhiloStarWars

extension Result {
    var value: T? {
        switch self {
        case .success(let value): return value
        case .failure: return nil
        }
    }
}
