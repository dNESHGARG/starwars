//
//  MockSession.swift
//  PhiloStarWarsTests
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
@testable import PhiloStarWars

class URLProtocolMock: URLProtocol {
    static var mock = [URL?: [AllDetailsModel]]()
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        if let url = request.url {
            if let data = URLProtocolMock.mock[url]{
                let archivedData = archive(data: data)
                self.client?.urlProtocol(self, didLoad: archivedData)
            }
        }
        
        // mark that we've finished
        self.client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() { }
}



func unarchive<T: Codable>(d: Data) -> T {
    guard d.count == MemoryLayout<T>.stride else {
        fatalError("Error!")
    }

    var w: T?
    d.withUnsafeBytes({(bytes: UnsafePointer<T>) -> Void in
        w = UnsafePointer<T>(bytes).pointee
    })
    
    return w!
}


extension URLSession: URLSessionProtocol {}

protocol URLSessionProtocol {
    func dataTask(
        with request: URLRequest,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> URLSessionDataTask
}

class MockURLSession: URLSessionProtocol {
    var dataTaskCallCount = 0
    func dataTask(
        with request: URLRequest,
        completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void
    ) -> URLSessionDataTask {
        var dataTaskCallCount = 0
        return URLSession.shared.dataTask(with: request)
    }
}
