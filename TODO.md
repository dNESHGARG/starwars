TODO -

1. Views could be more generalized.
Effort was put into making separate views for films, people, starships etc. Although they are
all using reusable components. But, I still feel that I can avoid lots of code and make something
more extensible and less clutter.

2. Image downloading is following the similar flow as Search and Details, but, I am not making
use  of the execute<> method defined in Worker protocol for Image download. It was added pretty late and I wasn't happy with Image service so wanted to keep it separate. Part of the 
reason is that the pexel api loads Photo data first, and from that we fetch the url for an image
and download it. I just combined all of that clutter in download method in ImageWorker.
But, it is still modular and all the garbage is in one function only.

3. Known Bug : This is what makes me most unhappy, I am still figuring it out.
The bug is related to navigation. In certain cases once we click a cell and come back to the previous view, we can't click the cell again. As of now I am not sure what's causing that, but that's something I am trying to figure out next. I am still not fully aware of all of the in's and outs of SwiftUI, but this seems more like non-ui bug. I will make a push to my gitlab url once I fix this. 
All other behavious seem to be working okay.

4. Unit Tests: I have worked on building the framework for tests with all kind of mocks. The AppModule has been mocked for search and details functionalities. Since the approach was protocol based, so it's not hacky and the only place where we alter some response is by not executing the default "execute" behavior of worker and definiing it for the mock. 
Also, Unit Tests coverage are no where close to what it should be.

5. UI Tests 

7. Implement Caching for requests and also Reachability for netowork connections.

8. As always, I will revise my code again and again, there are always pieces to refactor.
