//
//  ImageDirector.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

protocol ImageDirector {
    func load(image: Binding<LoadedView<UIImage>>, endpoint: Endpoint, completion: @escaping (Result<UIImage>) -> Void)
}

struct SwapImageDirector: ImageDirector {
    let worker: SwapImageWorker
    let state: AppState
    let cache = ImageCache()
    
    init(worker: SwapImageWorker,
         state: AppState) {
        self.worker = worker
        self.state = state
    }
    
    func load(image: Binding<LoadedView<UIImage>>, endpoint: Endpoint, completion: @escaping (Result<UIImage>) -> Void) {
        
        image.wrappedValue = .loading(last: image.wrappedValue.value)
        
        self.worker.load(endpoint: endpoint, width: 400) { result in
            switch result {
            case let .success(image):
                completion(Result.success(image))
            case let .failure(error):
                completion(Result.failure(.failureParsing(error)))
            }
        }
    }
}

extension URL {
    var imageCacheKey: ImageCacheKey { return absoluteString }
}

struct StubSwapImageDirector: ImageDirector {
    func load(image: Binding<LoadedView<UIImage>>, endpoint: Endpoint, completion: @escaping (Result<UIImage>) -> Void) {
        return completion(Result.failure(.invalidData))
    }
    
}
