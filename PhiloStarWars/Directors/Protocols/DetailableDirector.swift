//
//  DetailableDirector.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

protocol DetailableDirector: Director {
    var worker: Detailable { get }
    func details(with string: String,
                 endpoint: Endpoint,
                 completion: @escaping (LoadedView<AllDetailsModel>) -> Void)
}

extension DetailableDirector {
    func details(with string: String,
                 endpoint: Endpoint,
                 completion: @escaping (LoadedView<AllDetailsModel>) -> Void) {
       
        return worker.details(with: string, endpoint: endpoint) { model in
            switch model {
            case let .success(value):
                completion(.loaded(value))
            case let .failure(error):
                completion(.failed(error))
            }
        }
    }
}

