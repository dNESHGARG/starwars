//
//  LoadableDirector.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

protocol LoadableDirector: Director {
    var worker: Loadable { get }
    func loadAll(for endpoint: Endpoint)
}

extension LoadableDirector {
    func loadAll(for endpoint: Endpoint) {
        weak var weakAppState = state
        
        return worker.loadAll(for: endpoint) { model in
            switch model {
            case let .success(value):
                DispatchQueue.main.async {
                    weakAppState?.allResults = .loaded(value)
                }
            case let .failure(error):
                weakAppState?.allResults = .failed(error)
            }
        }
    }
}
