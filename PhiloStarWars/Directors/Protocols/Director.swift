//
//  DirectorProtocol.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/12/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

protocol Director {
    var state: AppState { get }
}
