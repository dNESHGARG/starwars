//
//  SearchableDirector.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

protocol SearchableDirector: Director {
    var worker: Searchable { get }
    func search(with string: String, endpoint: Endpoint)
    var historyManager: HistoryManager { get }
}

extension SearchableDirector {
    func search(with string: String, endpoint: Endpoint) {
         manageHistory(for: string)
        
        // execute search request.
        worker.search(with: string,
                      endpoint: endpoint) { model in
                        weak var weakAppState = self.state
                        switch model {
                        case let .success(value):
                            DispatchQueue.main.async {
                                weakAppState?.searchResults = .loaded(value)
                            }
                        case let .failure(error):
                            weakAppState?.searchResults = .failed(error)
                        }
        }
    }
    
    func reset() {
        state.searchResults = .notRequested
    }
    
    func searchFromHistory(with query: String) {
        state.searchTerm = query
    }
    
    private func manageHistory(for query: String) {
        historyManager.add(toHistory: query)
        state.history = historyManager.getHistory()
    }
}
