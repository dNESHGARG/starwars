//
//  SearchDirector.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct SearchDirector: SearchableDirector {
    var worker: Searchable
    var state: AppState
    
    var historyManager: HistoryManager = HistoryManager()
}
