//
//  HelperFunctions.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

func readableName(_ string: String) -> String {
    return (string as NSString).lastPathComponent
}

func makePlural(_ string: String) -> String {
    return ("\(string)s")
}

func makePathable(_ string: String) -> String {
    return ("\(string)/")
}

func makePluralAndPathable(_ string: String) -> String {
    /// endpoints people and species remain same even if its for plural usage.
    /// (English is funny :))
    return makePathable(makePlural(string))
}

func readJSONFromFile(fileName: String) -> Data? {
    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
        do {
            let fileUrl = URL(fileURLWithPath: path)
            let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            return data
        } catch {
            return nil
        }
    }
    return nil
}
