//
//  LoadedView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

enum LoadedView<T> {
    case notRequested
    case loaded(T)
    case loading(last: T?) // previous value
    case failed(NetworkError)
    
    var value: T? {
        switch self {
        case let .loaded(value): return value
        case let .loading(last): return last
        default: return nil
        }
    }
    
    var error: Error? {
        switch self {
        case let .failed(error): return error
        default: return nil
        }
    }
}

extension LoadedView: Equatable where T: Equatable {
    static func == (lhs: LoadedView<T>, rhs: LoadedView<T>) -> Bool {
        switch (lhs, rhs) {
        case (.notRequested, .notRequested): return true
        case let (.loading(lhsV), .loading(rhsV)): return lhsV == rhsV
        case let (.loaded(lhsV), .loaded(rhsV)): return lhsV == rhsV
        case let (.failed(lhsE), .failed(rhsE)):
            return lhsE.localizedDescription == rhsE.localizedDescription
        default: return false
        }
    }
}
