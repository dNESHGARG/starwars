//
//  CancelBag.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/10/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Combine
import Foundation

class CancelBag {
    var subscriptions = Set<AnyCancellable>()
}

extension AnyCancellable {
    static var cancelled: AnyCancellable {
        let cancellable = AnyCancellable({ })
        cancellable.cancel()
        return cancellable
    }
    
    func store(in cancelBag: CancelBag) {
        cancelBag.subscriptions.insert(self)
    }
}

extension Subscribers.Completion {
    var error: Failure? {
        switch self {
        case let .failure(error): return error
        default: return nil
        }
    }
}
