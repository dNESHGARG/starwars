//
//  DependencyInjector.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SwiftUI

/// Views are dependency injected with directors and app state.
struct DependencyInjector: ViewModifier {
    
    let appState: AppState
    let directors: DirectorContainer
    
    init(appState: AppState, directors: DirectorContainer) {
        self.appState = appState
        self.directors = directors
    }
    
    func body(content: Content) -> some View {
        content
            .environment(\.directors, directors)
            .environmentObject(appState)
    }
}
