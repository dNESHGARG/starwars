//
//  DirectorContainer.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

/// Container containing the handlers operating with View to make calls to fetch data.
/// Directors contain worker objects, which are abstract gateway for reading / writing data.
/// Provides access to a single data service, be that a web server or a local database
struct DirectorContainer: EnvironmentKey {
    
    /// Define all Directors here.
    /// Ideally we only need Search Director and Details Director, but we can
    /// easily expand the app to show all the resources and this is how we
    /// will expand the directors.
    /// This class is the single source of truth for all kind of directors for
    /// different feature sets.
    let searchDirector: SearchDirector
    let loadAllDirector: LoadAllDirector
    let detailsDirector: DetailsDirector
    let imageDirector: SwapImageDirector
    
    init(searchDirector: SearchDirector,
         loadAllDirector: LoadAllDirector,
         detailsDirector: DetailsDirector,
         imageDirector: SwapImageDirector) {
        self.searchDirector = searchDirector
        self.loadAllDirector = loadAllDirector
        self.detailsDirector = detailsDirector
        self.imageDirector = imageDirector
    }
    
    static var defaultValue: DirectorContainer {
        return DirectorContainer.mock()
    }
}

extension EnvironmentValues {
    var directors: DirectorContainer {
        get { self[DirectorContainer.self] }
        set { self[DirectorContainer.self] = newValue }
    }
}
