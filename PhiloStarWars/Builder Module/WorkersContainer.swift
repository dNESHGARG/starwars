//
//  WorkersContainer.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct WorkersContainer {
    let searchWorker: SearchWorker // search with query
    let allWorker: LoadAllWorker // All data for an endpoint. Eg. films
    let detailsWorker: DetailsWorker // Details for a resource
    let imageWorker: SwapImageWorker // fetch image from pexel api.
}
