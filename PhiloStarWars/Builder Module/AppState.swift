//
//  AppState.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

class AppState: ObservableObject {
    @Published var searchResults: LoadedView<SearchResponse> = .notRequested
 
    /// allResults and searchResults produce similar results, but keeping as
    /// separate properties as we can extend the app to load all as well as
    /// search in different tabs and can handle data propagation better.
    @Published var allResults: LoadedView<AllResponse> = .notRequested
    
    @Published var searchTerm: String = ""
    @Published var searchEndpoint: Endpoint = .people
    @Published var history = [String]()
}
