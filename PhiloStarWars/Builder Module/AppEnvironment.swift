//
//  AppEnvironment.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct AppEnvironment {
    let appState: AppState
    let directors: DirectorContainer
    let injectors: DependencyInjector
}

extension AppEnvironment {
    /*
     Bootstrap all dependencies and inject for further usage.
     */
    static func bootstrap() -> AppEnvironment {
        
        let appState = AppState()
        let session =  configuredURLSession()
        let workers = buildWorkers(session: session)
        let directors = buildDirectors(appState: appState, workers: workers)
        
        let dependencyInjector = buildDependencies(appState: appState, directors: directors)
        
        return AppEnvironment(appState: appState, directors: directors, injectors: dependencyInjector)
        
    }
}

extension AppEnvironment {
    private static func configuredURLSession() -> URLSession {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration)
    }
    
    private static func buildWorkers(session: URLSession) -> WorkersContainer {
        
        let searchWorker =  SearchWorker(session: session)
        let allWorker = LoadAllWorker(session: session)
        let detailsWorker = DetailsWorker(session: session)
        ///Interactor to fetch images.
        /// Using an API called pexel for images
        let imageWorker = SwapImageWorker(session: session)
        
        return WorkersContainer(searchWorker: searchWorker,
                                allWorker: allWorker,
                                detailsWorker: detailsWorker,
                                imageWorker: imageWorker)
    }
    
    private static func buildDirectors(appState: AppState, workers: WorkersContainer) -> DirectorContainer {
        
        let searchDirector = SearchDirector(worker: workers.searchWorker, state: appState)
        let loadAllDirector = LoadAllDirector(worker: workers.allWorker, state: appState)
        let detailsDirector = DetailsDirector(worker: workers.detailsWorker, state: appState)
        
        // Director to fetch images
        let imageDirector = SwapImageDirector(worker: workers.imageWorker, state: appState)
        
        return DirectorContainer(searchDirector: searchDirector,
                                 loadAllDirector: loadAllDirector,
                                 detailsDirector: detailsDirector,
                                 imageDirector: imageDirector)
    }
    
    static func buildDependencies(appState: AppState, directors: DirectorContainer) -> DependencyInjector {
        
        // A view modifier with EnvironmentObject - AppState and Environment - Directors.
        return DependencyInjector(appState: appState, directors: directors)
    }
}


