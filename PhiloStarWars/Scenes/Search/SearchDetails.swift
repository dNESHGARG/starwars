//
//  SearchDetails.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

// MARK: - Search Details
struct SearchDetailsView: View {
    @EnvironmentObject var appState: AppState
    @Environment(\.directors) var directors: DirectorContainer
    
    var result: AllDetailsModel
    var body: some View {
        DetailsSceneRouter(details: result, viewType: .people)
    }
}

// MARK: - Preview

struct SearchDetails_Previews: PreviewProvider {
    static var previews: some View {
        SearchDetailsView(result: AllDetailsModel.mock())
    }
}
