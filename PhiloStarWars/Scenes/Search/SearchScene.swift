//
//  PeopleSearchScene.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI
import Combine

struct SearchScene: View {
    let placeholder = "Search Star Wars Resources"
    @EnvironmentObject var appState: AppState
    @Environment(\.directors) var directors: DirectorContainer
    
    private let cancelBag = CancelBag()
    
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(text: $appState.searchTerm,
                          placeholder: placeholder,
                          onTextChanged: searchResources(with:))
                endpointSelectorView
                lineView
                GeometryReader { geometry in
                    self.content
                }
                .navigationBarTitle(Text("\($appState.searchEndpoint.wrappedValue.rawValue.capitalized)"))
            }
        }.onAppear {
            self.setDebounce()
        }
    }
    
    private var content: AnyView {
        switch appState.searchResults {
        case .notRequested: return AnyView(notRequestedView)
        case let .loading(last):
            let value = last?.results
            return AnyView(loadingView(value))
        case let .loaded(media):
            let value = media.results
            return AnyView(loadedView(value))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
    
    private var endpointSelectorView: some View {
        return SearchEndpointsScrollView()
            .frame(minWidth: 300.0, idealWidth: .infinity, maxWidth: .infinity, minHeight: 40.0, idealHeight: 40.0, maxHeight: 40.0, alignment: .leading)
            .padding(EdgeInsets(top: 0.0, leading: 5.0, bottom: 0.0, trailing: 5.0))
    }
    
    private var lineView: some View {
        return Rectangle().frame(minWidth: 300.0, idealWidth: .infinity, maxWidth: .infinity, minHeight: 1.0, idealHeight: 1.0, maxHeight: 1.0, alignment: .leading)
            .foregroundColor(Color.secondary)
    }
}

// MARK: - Actions
extension SearchScene {
    func searchResources(with searchText: String) { }
    
    func searchMedia(for searchText: String) {
        appState.searchResults = .loading(last: appState.searchResults.value)
        if !searchText.isEmpty {
            directors.searchDirector
                .search(with: searchText, endpoint: appState.searchEndpoint)
        } else {
            directors.searchDirector.reset()
        }
    }
    
    func setDebounce() {
        /// Execute query with delay of 0.5seconds after user has stopped typing.
        let scheduler: DispatchQueue = DispatchQueue(label: "SearchResources")
        appState.$searchTerm
            .dropFirst(1)
            .debounce(for: .seconds(0.5), scheduler: scheduler)
            .receive(on: RunLoop.main)
            .sink(receiveValue: searchMedia(for:))
            .store(in: cancelBag)
    }
}

// MARK: - Loading Content

private extension SearchScene {
    var notRequestedView: some View {
        return List(appState.history, id: \.self) { value in
            Text(value).onTapGesture(count: 1) {
                self.directors
                    .searchDirector
                    .searchFromHistory(with: value)
            }
            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
            .padding(10)
        }
    }
    
    func loadingView(_ previouslyLoaded: [AllDetailsModel]?) -> some View {
        VStack {
            ActivityIndicatorView().padding()
            previouslyLoaded.map {
                loadedView($0)
            }
        }
    }
    
    func failedView(_ error: Error) -> some View {
        ErrorView(error: error, retryAction: {
            self.searchMedia(for: self.appState.searchTerm)
        })
    }
}

// MARK: - Displaying Content

private extension SearchScene {
    func loadedView(_ results: [AllDetailsModel]) -> some View {
        isEmptyResult(results) ?
            AnyView(noResultsView()) : AnyView(resultsList(results))
    }
    
    private func resultsList(_ results: [AllDetailsModel]) -> some View {
        return List(results, id: \.self) { result in
            NavigationLink(
            destination: self.detailsView(result: result)){
                SearchCell(result: result)
            }
        }
    }
    
    private func noResultsView() -> some View {
        TitleLabel(text: "No Results Found").padding(20.0)
    }
    
    private func isEmptyResult(_ results: [AllDetailsModel] ) -> Bool {
        return (results.count == 0)
    }
    
    func detailsView(result: AllDetailsModel) -> some View {
        SearchDetailsView(result: result).environmentObject(appState)
    }
}

// MARK: - Preview

struct SearchScene_Previews: PreviewProvider {
    static var previews: some View {
        SearchScene()
    }
}

