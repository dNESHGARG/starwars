//
//  SearchCell.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

// MARK: - Search Details
struct SearchCell: View {
    var result: AllDetailsModel
    var title: String {
        if let name = result.name { return name }
        if let title = result.title { return title }
        return "N/A"
    }
    var body: some View {
        Text(title)
    }
}


// MARK: - Preview

struct SearchCell_Previews: PreviewProvider {
    static var previews: some View {
        SearchCell(result: AllDetailsModel.mock())
    }
}
