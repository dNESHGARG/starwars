//
//  DetailsSceneRouter.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct DetailsSceneRouter: View {
    var details: AllDetailsModel
    var viewType: Endpoint
    var body: some View {
        VStack {
            GeometryReader { geometry in
                self.content
            }
        }
    }
    
    init(details: AllDetailsModel, viewType: Endpoint) {
        self.details = details
        self.viewType = viewType
    }
    
    private var content: AnyView {
        switch viewType {
        case .people:
            return AnyView(PeopleDetails(model: details))
        case .film:
            return AnyView(FilmDetails(model: details))
        case .planet:
            return AnyView(PlanetDetails(model: details))
        case .species:
            return AnyView(SpeciesDetails(model: details))
        case .starship:
            return AnyView(StarshipDetails(model: details))
        case .vehicle:
            return AnyView(VehicleDetails(model: details))
        }
    }
}


// MARK: - Preview

struct DetailsSceneRouter_Previews: PreviewProvider {
    static var previews: some View {
        DetailsSceneRouter(details: AllDetailsModel.mock(), viewType: .people)
    }
}
