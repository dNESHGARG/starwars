//
//  SpeciesDetails.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct SpeciesDetails: View {
    var model: AllDetailsModel
    var body: some View {
        VStack {
            MediaArtWork(endpoint: .species)
            ScrollView(.vertical) {
                VStack(alignment: .leading, spacing: 15.0) {
                    SpeciesInfoView(info: model)
                    HomeworldView(info: model.homeworld)
                    CustomScrollViewContainer(model: model.people, title: "People")
                    CustomScrollViewContainer(model: model.films, title: "Films")
                    FootNoteView(footnote: model.url)
                }.padding(20)
            }
        }
    }
}

struct SpeciesInfoView: View {
    var info: AllDetailsModel
    
    var title: String { return info.name ?? info.title ?? "N/A" }
    var averageLifespan: String { return info.averageLifespan ?? "N/A" }
    var designation: String { return info.designation ?? "N/A" }
    var language: String { return info.language ?? "N/A" }
    var averageHeight: String { return info.averageHeight ?? "N/A" }
    var eyeColor: String { return info.eyeColor ?? "N/A" }
    var skinColor: String { return info.skinColor ?? "N/A" }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title).font(.largeTitle)
            HStack {
                TitleLabel(text:"Average Lifespan:")
                TextLabel(text:averageLifespan)
            }
            HStack{
                TitleLabel(text:"Designation:")
                TextLabel(text:designation)
            }
            HStack{
                TitleLabel(text:"Average Height:")
                TextLabel(text:averageHeight)
            }
            HStack{
                TitleLabel(text:"Language:")
                TextLabel(text:language)
            }
            HStack{
                TitleLabel(text:"Skin:")
                TextLabel(text:eyeColor)
                TitleLabel(text:"Eyes:")
                TextLabel(text:skinColor)
            }
            HStack{
                TitleLabel(text:"Skin:")
                TextLabel(text:eyeColor)
                TitleLabel(text:"Eyes:")
                TextLabel(text:skinColor)
            }
            
        }.foregroundColor(.black)
    }
}


// MARK: - Preview

struct SpeciesDetails_Previews: PreviewProvider {
    static var previews: some View {
        SpeciesDetails(model: AllDetailsModel.mock())
    }
}
