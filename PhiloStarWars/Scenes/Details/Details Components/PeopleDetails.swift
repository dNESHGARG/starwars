//
//  PeopleDetails.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct PeopleDetails: View {
    var model: AllDetailsModel
    var body: some View {
        VStack {
            MediaArtWork(endpoint: .people)
            ScrollView(.vertical) {
                VStack(alignment: .leading, spacing: 15.0) {
                    PeopleInfoView(info: model)
                    HomeworldView(info: model.homeworld, title: "Homeworld")
                    CustomScrollViewContainer(model: model.films, title: "Films")
                    CustomScrollViewContainer(model: model.species, title: "Species")
                    CustomScrollViewContainer(model: model.vehicles, title: "Vehicles")
                    CustomScrollViewContainer(model: model.starships, title: "Starships")
                    FootNoteView(footnote: model.url)
                }.padding(20)
            }
        }
    }
}

struct PeopleInfoView: View {
    var info: AllDetailsModel
    
    var title: String { return info.name ?? info.title ?? "N/A" }
    var dob: String { return info.birthYear ?? "N/A" }
    var gender: String { return info.gender ?? "N/A" }
    var height: String { return info.height ?? "N/A" }
    var mass: String { return info.mass ?? "N/A" }
    var eyeColor: String { return info.eyeColor ?? "N/A" }
    var skinColor: String { return info.skinColor ?? "N/A" }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title).font(.largeTitle)
            HStack {
                TitleLabel(text:"DOB:")
                TextLabel(text:dob)
                
                TitleLabel(text:"Gender:")
                TextLabel(text:gender)
            }
            HStack{
                TitleLabel(text:"Height:")
                TextLabel(text:height)
                
                TitleLabel(text:"Mass:")
                TextLabel(text:mass)
            }
            HStack{
                TitleLabel(text:"Skin:")
                TextLabel(text:eyeColor)
                
                TitleLabel(text:"Eyes:")
                TextLabel(text:skinColor)
            }
            
        }.foregroundColor(.black)
    }
}


// MARK: - Preview

struct PeopleDetails_Previews: PreviewProvider {
    static var previews: some View {
        PeopleDetails(model: AllDetailsModel.mock())
    }
}
