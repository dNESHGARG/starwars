//
//  PlanetDetails.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct PlanetDetails: View {
    var model: AllDetailsModel
    var body: some View {
        VStack {
            MediaArtWork(endpoint: .planet)
            ScrollView(.vertical) {
                VStack(alignment: .leading, spacing: 15.0) {
                    PlanetInfoView(info: model)
                    CustomScrollViewContainer(model: model.residents, title: "Residents")
                    CustomScrollViewContainer(model: model.films, title: "Films")
                    FootNoteView(footnote: model.url)
                }.padding(20)
            }
        }
    }
}

struct PlanetInfoView: View {
    var info: AllDetailsModel
    
    var title: String { return info.name ?? info.title ?? "N/A" }
    var rotationPeriod: String { return info.rotationPeriod ?? "N/A" }
    var orbitalPeriod: String { return info.orbitalPeriod ?? "N/A" }
    var diameter: String { return info.diameter ?? "N/A" }
    var climate: String { return info.climate ?? "N/A" }
    var gravity: String { return info.gravity ?? "N/A" }
    var terrain: String { return info.terrain ?? "N/A" }
    var surfaceWater: String { return info.surfaceWater ?? "N/A" }
    var population: String { return info.population ?? "N/A" }
    
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title).font(.largeTitle)
            VStack(alignment: .leading){
                TitleLabel(text:"Rotation Period")
                TextLabel(text:rotationPeriod)
                
                TitleLabel(text:"Orbital Period")
                TextLabel(text:orbitalPeriod)
                
                TitleLabel(text:"Diameter")
                TextLabel(text:diameter)
                
                TitleLabel(text:"Climate")
                TextLabel(text:climate)
                
                TitleLabel(text:"Gravity")
                TextLabel(text:gravity)
            }
            VStack(alignment: .leading) {
                TitleLabel(text:"Terrain")
                TextLabel(text:terrain)
                
                TitleLabel(text:"Surface Water")
                TextLabel(text:surfaceWater)
                
                TitleLabel(text:"Population")
                TextLabel(text:population)
            }
        }.foregroundColor(.black)
    }
}


// MARK: - Preview

struct PlanetDetails_Previews: PreviewProvider {
    static var previews: some View {
        PlanetDetails(model: AllDetailsModel.mock())
    }
}
