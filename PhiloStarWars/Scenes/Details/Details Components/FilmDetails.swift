//
//  FilmDetails.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct FilmDetails: View {
    var model: AllDetailsModel
    var body: some View {
        VStack {
            MediaArtWork(endpoint: .film)
            ScrollView(.vertical) {
                VStack(alignment: .leading, spacing: 15.0) {
                    FilmInfoView(info: model)
                    OpeningCrawlView(info: model.openingCrawl)
                    CustomScrollViewContainer(model: model.characters, title: "Characters")
                    CustomScrollViewContainer(model: model.planets, title: "Planets")
                    CustomScrollViewContainer(model: model.vehicles, title: "Vehicles")
                    CustomScrollViewContainer(model: model.starships, title: "Starships")
                    CustomScrollViewContainer(model: model.species, title: "Species")
                    FootNoteView(footnote: model.url)
                }.padding(20)
            }
        }
    }
}

struct OpeningCrawlView: View {
    var info: String?
    var openingCrawl: String { return info ?? "N/A"}
    
    var body: some View {
        VStack(alignment: .leading) {
            TitleLabel(text:"Opening Crawl")
            TextLabel(text:openingCrawl)
        }
    }
}

struct FilmInfoView: View {
    var info: AllDetailsModel
    
    var title: String { return info.name ?? info.title ?? "N/A" }
    var director: String { return info.director ?? "N/A" }
    var producer: String { return info.producer ?? "N/A" }
    var releaseDate: String { return info.releaseDate ?? "N/A" }
    
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title).font(.largeTitle)
            VStack(alignment: .leading) {
                TitleLabel(text:"Director:")
                TextLabel(text:director)
                
                TitleLabel(text:"Producer:")
                TextLabel(text:producer)
                
                TitleLabel(text:"Release Date:")
                TextLabel(text:releaseDate)
            }
        }.foregroundColor(.black)
    }
}


// MARK: - Preview

struct FilmDetails_Previews: PreviewProvider {
    static var previews: some View {
        FilmDetails(model: AllDetailsModel.mock())
    }
}
