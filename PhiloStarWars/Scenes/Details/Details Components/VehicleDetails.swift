//
//  VehicleDetails.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct VehicleDetails: View {
    var model: AllDetailsModel
    var body: some View {
        VStack {
            MediaArtWork(endpoint: .vehicle)
            ScrollView(.vertical) {
                VStack(alignment: .leading, spacing: 15.0) {
                    VehicleInfoView(info: model)
                    CustomScrollViewContainer(model: model.pilots, title: "Pilots")
                    CustomScrollViewContainer(model: model.films, title: "Films")
                    FootNoteView(footnote: model.url)
                }.padding(20)
            }
        }
    }
}

struct VehicleInfoView: View {
    var info: AllDetailsModel
    
    var title: String { return info.name ?? info.title ?? "N/A" }
    var model: String { return info.model ?? "N/A" }
    var manufacturer: String { return info.manufacturer ?? "N/A" }
    var length: String { return info.length ?? "N/A" }
    var maxAtmospheringSpeed: String { return info.maxAtmospheringSpeed ?? "N/A" }
    var crew: String { return info.crew ?? "N/A" }
    var passengers: String { return info.passengers ?? "N/A" }
    var cargoCapacity: String { return info.cargoCapacity ?? "N/A" }
    var consumables: String { return info.consumables ?? "N/A" }
    var vehicleClass: String { return info.vehicleClass ?? "N/A" }
    
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title).font(.largeTitle)
            VStack(alignment: .leading) {
                TitleLabel(text:"Model")
                TextLabel(text:model)
                
                TitleLabel(text:"Manufacturer")
                TextLabel(text:manufacturer)
                
                TitleLabel(text:"Length")
                TextLabel(text:length)
                
                TitleLabel(text:"Max Atmosphering Speed")
                TextLabel(text:maxAtmospheringSpeed)
                
                TitleLabel(text:"Crew")
                TextLabel(text:crew)
            }
            VStack(alignment: .leading) {
                TitleLabel(text:"Passengers")
                TextLabel(text:passengers)
                
                TitleLabel(text:"Cargo Capacity")
                TextLabel(text:cargoCapacity)
                
                TitleLabel(text:"Consumables")
                TextLabel(text:consumables)
                
                TitleLabel(text:"Vehicle Class")
                TextLabel(text:vehicleClass)
            }
        }.foregroundColor(.black)
    }
}


// MARK: - Preview

struct VehicleDetail_Previews: PreviewProvider {
    static var previews: some View {
        VehicleDetails(model: AllDetailsModel.mock())
    }
}
