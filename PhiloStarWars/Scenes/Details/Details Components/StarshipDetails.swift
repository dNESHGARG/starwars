//
//  StarshipDetails.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct StarshipDetails: View {
    var model: AllDetailsModel
    var body: some View {
        VStack {
            MediaArtWork(endpoint: .starship)
            ScrollView(.vertical) {
                VStack(alignment: .leading, spacing: 15.0) {
                    StarshipInfoView(info: model)
                    CustomScrollViewContainer(model: model.pilots, title: "Pilots")
                    CustomScrollViewContainer(model: model.films, title: "Films")
                    FootNoteView(footnote: model.url)
                }.padding(20)
            }
        }
    }
}

struct StarshipInfoView: View {
    var info: AllDetailsModel
    
    var title: String { return info.name ?? info.title ?? "N/A" }
    var model: String { return info.model ?? "N/A" }
    var manufacturer: String { return info.manufacturer ?? "N/A" }
    var length: String { return info.length ?? "N/A" }
    var maxAtmospheringSpeed: String { return info.maxAtmospheringSpeed ?? "N/A" }
    var crew: String { return info.crew ?? "N/A" }
    var passengers: String { return info.passengers ?? "N/A" }
    var cargoCapacity: String { return info.cargoCapacity ?? "N/A" }
    var consumables: String { return info.consumables ?? "N/A" }
    var starshipClass: String { return info.starshipClass ?? "N/A" }
    
    var costInCredits: String { return info.costInCredits ?? "N/A" }
    var hyperdriveRating: String { return info.hyperdriveRating ?? "N/A" }
    var mglt: String { return info.mglt ?? "N/A" }
    
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title).font(.largeTitle)
            VStack(alignment: .leading) {
                TitleLabel(text:"Model")
                TextLabel(text:model)
                
                TitleLabel(text:"Manufacturer")
                TextLabel(text:manufacturer)
                
                TitleLabel(text:"Length")
                TextLabel(text:length)
                
                TitleLabel(text:"Max Atmosphering Speed")
                TextLabel(text:maxAtmospheringSpeed)
                
                TitleLabel(text:"Crew")
                TextLabel(text:crew)
            }
            VStack(alignment: .leading) {
                TitleLabel(text:"Passengers")
                TextLabel(text:passengers)
                
                TitleLabel(text:"Cargo Capacity")
                TextLabel(text:cargoCapacity)
                
                TitleLabel(text:"Consumables")
                TextLabel(text:consumables)
            }
            VStack(alignment: .leading) {
                TitleLabel(text:"Starship Class")
                TextLabel(text:starshipClass)
                
                TitleLabel(text:"Cost In Credits")
                TextLabel(text:costInCredits)
                
                TitleLabel(text:"Hyperdrive Rating")
                TextLabel(text:hyperdriveRating)
                
                TitleLabel(text:"MGLT")
                TextLabel(text:mglt)
            }
        }.foregroundColor(.black)
    }
}


// MARK: - Preview

struct StarshipDetails_Previews: PreviewProvider {
    static var previews: some View {
        StarshipDetails(model: AllDetailsModel.mock())
    }
}
