//
//  DetailedInfoView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

protocol DetailedInfoProtocol {
    /// For routing to desired endpoint.
    var endPoint: Endpoint { get }
    var itemValue: String { get }
}

struct DetailedInfoView: View, DetailedInfoProtocol {
    @EnvironmentObject var appState: AppState
    @Environment(\.directors) var directors: DirectorContainer
    @State var data: LoadedView<AllDetailsModel> = .loading(last: nil)
    
    var itemEndpoint: String /// String from TitleLabel. Mapped to endpoint.
    var itemValue: String
    
    var endPoint: Endpoint {
        return Endpoint(rawValue: itemEndpoint) ?? Endpoint.people
    }
    
    var result: AllDetailsModel {
        switch data {
        case let .loaded(value):
            return value
        default:
            return AllDetailsModel.mock()
        }
    }
    
    var body: some View {
        GeometryReader { geometry in
            self.content
                .onAppear { self.loadDetails() }
        }
    }
    
    private var content: AnyView {
        switch data {
        case .notRequested: return AnyView(notRequestedView)
        case let .loading(last):
            return AnyView(loadingView(last))
        case let .loaded(value):
            return AnyView(loadedView(value))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
    
    
    init(endPoint: String, loadItem: String) {
        self.itemEndpoint = endPoint
        self.itemValue = loadItem
    }
}

// MARK: - Loading Content

private extension DetailedInfoView {
    var notRequestedView: some View {
        VStack(alignment: .leading, spacing: 15.0) {
            Text("Not Requested").foregroundColor(.gray).font(.title)
            ActivityIndicatorView().padding()
        }.padding(20)
    }
    
    func isPrevLoaded(_ previouslyLoaded: AllDetailsModel?) -> Bool {
        if let _ = previouslyLoaded {
            return true
        }
        
        return false
    }
    
    func loadingView(_ previouslyLoaded: AllDetailsModel?) -> some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Loading...").foregroundColor(.gray).font(.title)
            ActivityIndicatorView().padding()
            if (isPrevLoaded(previouslyLoaded)) {
                DetailsSceneRouter(details: previouslyLoaded!, viewType: endPoint)
            }
        }.padding(10)
    }
    
    func failedView(_ error: Error) -> some View {
        VStack {
            Text("An Error Occured")
                .font(.title)
            Text(error.localizedDescription)
                .font(.callout)
                .multilineTextAlignment(.center)
                .padding(.bottom, 40).padding()
        }
    }
}

// MARK: - Displaying Content

private extension DetailedInfoView {
    func loadedView(_ result: AllDetailsModel) -> some View {
        DetailsSceneRouter(details: result, viewType: endPoint)
    }
    
    func detailsView(result: AllDetailsModel) -> some View {
        SearchDetailsView(result: result).environmentObject(appState)
    }
}


extension DetailedInfoView {
    private func loadDetails() {
        self.directors.detailsDirector
            .details(with: itemValue, endpoint: endPoint) { model in
                self.data = model
        }
    }
}

// MARK: - Preview

struct DetailedInfoView_Previews: PreviewProvider {
    static var previews: some View {
        DetailedInfoView(endPoint: "films", loadItem: "3")
    }
}
