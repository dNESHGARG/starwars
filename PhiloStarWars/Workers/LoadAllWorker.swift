//
//  LoadAllWorker.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct LoadAllWorker: Loadable {
    typealias AllModel = AllResponse
    var service: Service
    
    init(session: URLSession) {
        self.service = LoadAllService(session: session)
    }
}
