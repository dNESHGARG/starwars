//
//  DetailsWorker.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct DetailsWorker: Detailable {
    typealias DetailsModel = AllDetailsModel
    var service: Service
    
    init(session: URLSession) {
        self.service = DetailsService(session: session)
    }
}
