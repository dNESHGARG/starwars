//
//  ImageWorker.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

import Combine
import SwiftUI

protocol ImageWorker: Service {
    func load(endpoint: Endpoint, width: Int, completion: @escaping (Result<UIImage>) -> Void)
}

struct SwapImageWorker: ImageWorker {
    var session: URLSession
    
    var baseURL: BaseURL = BaseURL.image
    
    var httpMethod: HTTPMethod = .get
    
    var query: [String : String]?
    
    var headers: [String : String]?
    
    let bgQueue = DispatchQueue(label: "bg_parse_queue")
    
    
    init(session: URLSession) {
        self.session = session
    }
}

extension SwapImageWorker {
    
    func load(endpoint: Endpoint, width: Int, completion: @escaping (Result<UIImage>) -> Void) {
        return download(endpoint: endpoint) { result in
            switch result {
            case let .success(image):
                completion(Result.success(image))
            case .failure:
                completion(Result.failure(.invalidData))
            }
        }
    }
}


/// TODO: Last Minute Implementation: Not proud of this implementation.
/// Need to abstract it and use the worker execute method. But, we can see that
/// the modularity is still maintained.
extension SwapImageWorker {
    private func download(endpoint: Endpoint, completion: @escaping(Result<UIImage>) ->Void){
        var rawValue: String
        switch endpoint {
        case .starship:
            rawValue = "spaceship" /// pexel api doesn't give any response for starship.
        default:
            rawValue = endpoint.rawValue
        }
        
        let urlString = ("\(BaseURL.image.value)/search?query=\(rawValue)")
        var request = URLRequest(url: URL(string: urlString)!)
        
        /// TODO: Must be stored in keychain.
        let accessToken = "563492ad6f91700001000001808dd0793e7547df8b01b70bee80db84"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(Result.failure(.unknownError(error)))
            }
            
            guard let response = response as? HTTPURLResponse  else {
                completion(Result.failure(.noResponse))
                return
            }
            
            switch response.statusCode {
            case 200 ... 299:
                print("success - \(response.statusCode)")
            default:
                completion(Result.failure(.invalidResponse(response.statusCode)))
            }
            
            guard let data = data else {
                completion(Result.failure(.invalidData))
                return
            }
            
            do {
                let decoded = try JSONDecoder().decode(ImageResults.self, from: data)
                /// Check if photos have any results
                /// If result are found, download image from url
                /// complete with completion closure.
                guard let photos = decoded.photos else {
                    completion(Result.failure(.invalidData))
                    return
                }
                
                if photos.count == 0 {
                    completion(Result.failure(.invalidURL))
                    return
                }
                
                let number = Int.random(in: 0 ..< photos.count)
                
                
                if number < photos.count {
                    let photo = photos[number]
                    
                    if let src = photo.src, let medium = src["medium"] {
                        let url = URL(string: medium)
                        
                        do {
                            let data = try Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                let uiImage = UIImage(data: data)!
                                completion(Result.success(uiImage))
                            }
                        } catch {
                            completion(Result.failure(.invalidData))
                        }
                    }
                }
                
            } catch {
                completion(Result.failure(.failureParsing(error)))
            }
        }
        task.resume()
    }
}
