//
//  SearchWorker.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct SearchWorker: Searchable {
    typealias SearchModel = SearchResponse
    var service: Service
    
    init(session: URLSession) {
           self.service = SearchService(session: session)
    }
}
