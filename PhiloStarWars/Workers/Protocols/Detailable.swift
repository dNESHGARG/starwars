//
//  Detailable.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/12/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

protocol Detailable: Worker {
    func details(with searchString: String,
                 endpoint: Endpoint,
                 completion: @escaping(Result<AllDetailsModel>) -> Void)
}

extension Detailable {
    func details(with searchString: String, endpoint: Endpoint, completion: @escaping(Result<AllDetailsModel>) -> Void)  {
        let request = service.makeRequest(for : service,
                                          endpoint: endpoint,
                                          serviceType: .detail,
                                          item: searchString)!
        
        return execute(request, service.session) { (result: Result<AllDetailsModel>) in
                 switch result {
                 case let .success(model):
                     completion(Result.success(model))
                 case let .failure(error):
                     completion(Result.failure(error))
                 }
             }
    }
}
