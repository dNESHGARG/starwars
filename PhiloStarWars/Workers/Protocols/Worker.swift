//
//  Worker.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/12/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

protocol Worker {
    var service: Service { get }
}
