//
//  Loadable.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/12/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

protocol Loadable: Worker {
    func loadAll(for endpoint: Endpoint, completion: @escaping(Result<AllResponse>) -> Void)
}

extension Loadable {
    func loadAll(for endpoint: Endpoint, completion: @escaping(Result<AllResponse>) -> Void)  {
        let request = service.makeRequest(for : service,
                                          endpoint: endpoint,
                                          serviceType: .loadAll,
                                          item:nil)!
        return execute(request, service.session) { (result: Result<AllResponse>) in
            switch result {
            case let .success(model):
                completion(Result.success(model))
            case let .failure(error):
                completion(Result.failure(error))
            }
        }
    }
}
