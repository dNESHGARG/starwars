//
//  Searchable.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/10/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import Combine

protocol Searchable: Worker {
    func search(with searchString: String,
                endpoint: Endpoint,
                completion: @escaping(Result<SearchResponse>) -> Void)
}

extension Searchable {
    func search(with searchString: String,
                endpoint: Endpoint,
                completion: @escaping(Result<SearchResponse>) -> Void) {
        let request = service.makeRequest(for : service,
                                          endpoint: endpoint,
                                          serviceType: .search,
                                          item: searchString)!
        
        return execute(request, service.session) { (result: Result<SearchResponse>) in
            switch result {
            case let .success(model):
                completion(Result.success(model))
            case let .failure(error):
                completion(Result.failure(error))
            }
        }
    }
}
