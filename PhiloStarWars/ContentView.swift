//
//  ContentView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/8/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    private let injectors: DependencyInjector
    
    init(injector: DependencyInjector) {
        self.injectors = injector
    }
    
    var body: some View {
        return realContent
            .modifier(injectors)
    }
    
    private var realContent: some View {
        SearchScene().environmentObject(injectors.appState)
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView(injector: DependencyInjector(appState: AppState(), directors: DirectorContainer(peopleDirector: PeopleDirector.mock(), detailsDirector: DetailedInfoDirector.mock())))
//    }
//}
