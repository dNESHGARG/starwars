////
////  Service.swift
////  PhiloStarWars
////
////  Created by Dinesh Garg on 6/9/20.
////  Copyright © 2020 Dinesh Garg. All rights reserved.
////
//
import Foundation

enum ServiceType {
    case search
    case loadAll
    case detail
}

protocol Service {
    /// Ideally we can configure session in Service class only, but just keeping
    /// it settable if a request needs to initialize its own session.
    
    var session: URLSession { get set }
    var baseURL: BaseURL { get set }
    var httpMethod: HTTPMethod { get set }
    var query: [String: String]? { get set }
    var headers: [String: String]? { get }
    var bgQueue: DispatchQueue { get }
}

extension Service {
    private func makeEndpoint(serviceType: ServiceType,
                              endPoint: Endpoint,
                              item: String?) -> String {
        var searchString = ""
        if let _ = item {
            let escapedString = item!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            searchString = escapedString!
        }
        
        switch serviceType {
        case .search:
            switch endPoint {
                /// we can also construct query, but only query we have is for search.
            /// so, keeping it simple.
            case .people, .species:
                return "\(makePathable(endPoint.rawValue))?search=\(searchString)"
            default:
                return "\(makePluralAndPathable(endPoint.rawValue))?search=\(searchString)"
            }
        case .loadAll:
            switch endPoint {
                /// we can also construct query, but only query we have is for search.
            /// so, keeping it simple.
            case .people, .species:
                return "\(makePathable(endPoint.rawValue))"
            default:
                return "\(makePluralAndPathable(endPoint.rawValue))"
            }
        case .detail:
            switch endPoint {
            case .people, .species:
                return "\(makePathable(endPoint.rawValue))\(searchString)"
            default:
                return "\(makePluralAndPathable(endPoint.rawValue))\(searchString)/"
            }
        }
    }
    
    func makeRequest(for service: Service,
                     endpoint: Endpoint,
                     serviceType:ServiceType,
                     item: String?) -> URLRequest? {
        var urlPath = "\(service.baseURL.value)"
        urlPath.append(makeEndpoint(serviceType: serviceType,
                                    endPoint: endpoint,
                                    item: item))
        guard let url = URL(string: urlPath) else {
            return nil
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.get.rawValue
        
        let allHeaders = [
            "Accept": "application/json",
            "application/json": "Content-Type"
        ]
        
        request.allHTTPHeaderFields = allHeaders
        
        return request
    }
}
