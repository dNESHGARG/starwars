//
//  SearchService.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct SearchService: Service {
    var session: URLSession
    var baseURL: BaseURL = .swap(.prod)
    var httpMethod: HTTPMethod = .get
    
    var query: [String : String]?
    
    var headers: [String : String]?
    
    let bgQueue = DispatchQueue(label: "bg_parse_queue")
    
    init(session: URLSession) {
        self.session = session
    }
}
