//
//  Endpoint.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/16/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

enum Endpoint: String, CaseIterable {
    case film = "film"
    case people = "people"
    case planet = "planet"
    case species = "species"
    case starship = "starship"
    case vehicle = "vehicle"
    
    init?(rawValue: String) {
        switch rawValue.lowercased() {
        case "films", "film": self = .film
        case "people", "residents", "pilots", "pilot", "resident": self = .people
        case "planets", "planet", "homeworld", "homeworlds": self = .planet
        case "species": self = .species
        case "starships", "starship": self = .starship
        case "vehicles", "vehicle": self = .vehicle
        default:
            self = .people
        }
    }
    
    static var allEndpoints: [String] {
        var data: [String] = []
        for val in Endpoint.allCases {
            data.append(val.rawValue.capitalized)
        }
        return data
    }
}

enum BaseURL {
    case swap(APIEnvironment)
    case image
    
    var value: String {
        switch self {
        case let .swap(env):
            switch env {
            case .prod:
                return "https://swapi.dev/api/"
            default:
                return "https://swapi.dev/api/"
            }
        case .image:
            return "https://api.pexels.com/v1"
        }
    }
}

enum APIEnvironment {
    case mock
    case prod
    case test
}
