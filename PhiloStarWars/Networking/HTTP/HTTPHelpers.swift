//
//  HTTPHelpers.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    /// We ar enot using anyhting else othe than GET for our use case, still
    /// good practice to prepare other methods too.
    case post = "POST"
    case update = "UPDATE"
    case patch = "PATCH"
}

typealias HTTPCode = Int
typealias HTTPCodes = Range<HTTPCode>

extension HTTPCodes {
    static let success = 200 ..< 300
}
