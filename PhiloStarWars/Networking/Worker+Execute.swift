//
//  Worker+Execute.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/15/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation
import SystemConfiguration

enum Result<T> {
    case success(T)
    case failure(NetworkError)
}

extension Worker {
    func execute<T: Decodable>(_ request: URLRequest, _ session: URLSession, _ decoder: JSONDecoder = JSONDecoder(), completion: @escaping(Result<T>) -> Void) {
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(Result.failure(.unknownError(error)))
            }
            
            guard let response = response as? HTTPURLResponse  else {
                completion(Result.failure(.noResponse))
                return
            }
            
            switch response.statusCode {
            case 200 ... 299:
                print("success - \(response.statusCode)")
            default:
                completion(Result.failure(.invalidResponse(response.statusCode)))
            }
            
            guard let data = data else {
                completion(Result.failure(.invalidData))
                return
            }
            
            do {
                let decoded = try JSONDecoder().decode(T.self, from: data)
                completion(Result.success(decoded))
            } catch {
                completion(Result.failure(.failureParsing(error)))
            }
        }
        task.resume()
    }
}

