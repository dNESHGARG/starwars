//
//  HistoryManager.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/15/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

class HistoryManager {
    static let size = 10
    
    var history = Queue<String>()
    
    func add(toHistory query: String) {
        let queueArray = history.getAll()
        
        // Check for duplicates.
        let value: String? = query
        if queueArray.contains(value) { return }
        
        if (queueArray.count == HistoryManager.size) {
            history.dequeue()
        }
        
        history.enqueue(query)
    }
    
    func getHistory() -> [String] {
        let output = history.getAll().compactMap { $0 }
        return output
    }
}
