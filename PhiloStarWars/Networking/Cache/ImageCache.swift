//
//  ImageCache.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

import UIKit
import Combine

typealias ImageCacheKey = String

enum CacheResult<T>: Error {
    case success(T)
    case failure(CacheError)
}

protocol ImageCacheWorker {
    func cache(image: UIImage, key: ImageCacheKey)
    func cachedImage(for key: ImageCacheKey, completion: @escaping (CacheResult<UIImage>) -> Void)
    func purgeCache()
}

enum CacheError: Error {
    case imageIsMissing
    case responseIsMissing(String)
    
    var localizedDescription: String {
        switch self {
        case .imageIsMissing:
            return ("Image is missing in cache")
        case let .responseIsMissing(url):
            return ("Response is missing in cache for url: \(url)")
        }
    }
}

// MARK: - ImageCacheWorker

struct ImageCache: ImageCacheWorker {
    
    private let cache = NSCache<NSString, UIImage>()
    
    func cache(image: UIImage, key: ImageCacheKey) {
        cache.setObject(image, forKey: key as NSString, cost: image.estimatedSizeInKB)
    }
    
    func cachedImage(for key: ImageCacheKey,
                     completion: @escaping (CacheResult<UIImage>) -> Void) {
        guard let image = cache.object(forKey: key as NSString) else {
            completion(CacheResult.failure(.imageIsMissing))
            return
        }
        // emits the value to subscribers only once, and then finishes.
        completion(CacheResult.success(image))
    }
    
    func purgeCache() {
        cache.removeAllObjects()
    }
}

// MARK: - Image size

private extension UIImage {
    var estimatedSizeInKB: Int {
        let bytesPerRow = Int(size.width * scale) * 4
        let numberOfRows = Int(size.height * scale)
        return bytesPerRow / 1024 * numberOfRows
    }
}
