//
//  NetworkError.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case noResponse
    case invalidResponse(Int)
    case failureParsing(Error)
    case unknownError(Error)
    case invalidData
    
     var localizedDescription: String {
        switch self {
        case let .invalidResponse(statuscode):
            return ("Found invalid response with statuscode - \(statuscode)")
        case let .failureParsing(parsingerror):
            return ("failed to parse - \(parsingerror.localizedDescription)")
        case let .unknownError(error):
            return ("unknown error occured - \(error.localizedDescription)")
        default:
            return self.localizedDescription
        }
    }
}
