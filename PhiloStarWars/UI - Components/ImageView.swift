//
//  ImageView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct ImageView: View {
    var name: String?
    var body: some View {
        Image("placeholder_species")
            .resizable()
            .frame(minWidth: 320.0, idealWidth: .infinity, maxWidth: .infinity, minHeight: 200.0, idealHeight: 200.0, maxHeight: 200.0, alignment: .center)
        .clipped()
    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        ImageView(name: "placeholder_species")
    }
}
