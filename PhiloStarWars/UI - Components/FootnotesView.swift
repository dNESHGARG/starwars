//
//  FootNoteView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

// MARK: - Search Details
struct FootNoteView: View {
    var footnote: String
    var body: some View {
        Text(footnote).foregroundColor(.gray).font(.footnote)
    }
}

struct FootNoteView_Previews: PreviewProvider {
    static var previews: some View {
        FootNoteView(footnote: "footnote_url")
    }
}
