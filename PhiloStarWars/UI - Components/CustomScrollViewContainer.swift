//
//  CustomScrollViewContainer.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct CustomScrollViewContainer: View {
    var model: [String]?
    var title: String
    var body: some View {
        VStack(alignment: .leading) {
            TitleLabel(text:title)
            CustomScrollView(data: model, endpoint: title)
        }
    }
}

struct CustomScrollViewContainer_Previews: PreviewProvider {
    static var previews: some View {
        CustomScrollViewContainer(model: AllDetailsModel.mock().vehicles, title: "Vehicles")
    }
}
