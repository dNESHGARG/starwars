//
//  CustomScrollView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct CustomScrollView: View {
    @EnvironmentObject var appState: AppState
    @Environment(\.directors) var directors: DirectorContainer
    
    var data: [String]?
    var results: [String] {
        return data ?? []
    }
    var endpoint: String = "film"
    var body: some View {
        VStack {
            if(results.count > 0) {
                ScrollView(.horizontal) {
                    HStack(spacing: 10) {
                        ForEach(results, id: \.self) {
                            self.navigationLinkView($0,endpoint: self.endpoint)
                        }
                    }
                }
            } else {
                AnyView(Text("N/A").foregroundColor(.gray))
            }
        }
    }
    
    func navigationLinkView(_ text: String, endpoint: String) -> some View {
        NavigationLink(destination: detailsView(endPoint: endpoint,
                                                loadItem: text)) {
                                                    textView(text: text)
        }
    }
    
    func detailsView(endPoint: String, loadItem: String) -> some View {
        DetailedInfoView(endPoint: endPoint,
                         loadItem: readableName(loadItem)).environmentObject(appState)
    }
    
    func textView(text: String) -> some View {
        Text(readableName(text))
            .foregroundColor(.white)
            .font(.headline)
            .frame(width: 80, height: 80)
            .background(Color.gray)
    }
}

struct CustomScrollView_Previews: PreviewProvider {
    static var previews: some View {
        CustomScrollView(data: AllDetailsModel.mock().films)
    }
}
