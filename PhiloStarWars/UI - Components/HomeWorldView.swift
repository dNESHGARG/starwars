//
//  HomeWorldView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct HomeworldView: View {
    var info: String?
    var title: String = "Homeworld"
    
    var model: [String] {
        guard let info = info else {
            return []
        }
        return [info]
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            TitleLabel(text:title)
            CustomScrollView(data: model, endpoint: title)
        }
    }
}

struct HomeWorldView_Previews: PreviewProvider {
    static var previews: some View {
        HomeworldView(info: "home_world")
    }
}
