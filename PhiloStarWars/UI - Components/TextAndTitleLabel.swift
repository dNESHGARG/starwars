//
//  TextAndTitleLabel.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct TextLabel: View {
    var text: String
    var body: some View {
        Text(text)
    }
}

struct TitleLabel: View {
    var text: String
    var body: some View {
        Text(capitalize(text))
            .foregroundColor(.gray)
            .font(.subheadline)
    }
}

func capitalize(_ string: String) -> String {
    return string.uppercased()
}
