//
//  MediaArtWork.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI
import Combine
import WebKit

struct MediaArtWork: View {
    
    let endpoint: Endpoint
    @EnvironmentObject var appState: AppState
    @Environment(\.directors) var directors: DirectorContainer
    
    @State private var image: LoadedView<UIImage>
    
    private let cancelBag = CancelBag()
    
    init(endpoint: Endpoint = .planet, image: LoadedView<UIImage> = .notRequested) {
        self.endpoint = endpoint
        self._image = .init(initialValue: image)
    }
    
    var body: some View {
        content.frame(minWidth: 320, idealWidth: .infinity, maxWidth: .infinity, minHeight: 300, idealHeight: 300, maxHeight: 300, alignment: .center)
    }
    
    private var content: AnyView {
        switch image {
        case .notRequested: return AnyView(notRequestedView)
        case .loading: return AnyView(loadingView)
        case let .loaded(image): return AnyView(loadedView(image))
        case let .failed(error): return AnyView(failedView(error))
        }
    }
}

// MARK: - Side Effects

private extension MediaArtWork {
    func loadImage() {
        directors.imageDirector.load(image: $image, endpoint: self.endpoint) { (result: Result<UIImage>) in
            switch result {
            case let .success(image):
                DispatchQueue.main.async {
                    self.image = .loaded(image)
                }
            case let .failure(error):
                DispatchQueue.main.async {
                    self.image = .failed(error)
                }
            }
        }
    }
}

// MARK: - Content

private extension MediaArtWork {
    var notRequestedView: some View {
        Text("").onAppear {
            self.loadImage()
        }
    }
    
    var loadingView: some View {
        ActivityIndicatorView()
    }
    
    func failedView(_ error: Error) -> some View {
        Text("Unable to load image")
            .font(.footnote)
            .multilineTextAlignment(.center)
            .padding()
    }
    
    func loadedView(_ image: UIImage) -> some View {
        Image(uiImage: image).resizable()
            .aspectRatio(contentMode: .fit)
            .clipped()
    }
}
