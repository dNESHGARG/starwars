//
//  SearchEndpointsScrollView.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 7/3/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import SwiftUI

struct SearchEndpointsScrollView: View {
    @EnvironmentObject var appState: AppState
    
    var body: some View {
        VStack {
            if(Endpoint.allEndpoints.count > 0) {
                ScrollView(.horizontal) {
                    HStack(spacing: 5) {
                        ForEach(Endpoint.allEndpoints, id: \.self) { val in
                            EndpointItemView(text: val)
                        }
                    }
                }
            } else {
                Text("People")
            }
        }
    }
}

struct EndpointItemView: View {
    @EnvironmentObject var appState: AppState
    let text: String
    var body: some View {
        Text(text)
            .padding(EdgeInsets(top: 4.0, leading: 10.0, bottom: 4.0, trailing: 10.0))
            .foregroundColor(Color.gray)
            .gesture(TapGesture().onEnded({ _ in
                self.appState.searchEndpoint = Endpoint.init(rawValue: self.text) ?? .people
                
                /// Since searchTerm is an environment variable Switching between
                /// endpoints will make a network call with existing searchTerm.
                /// Kind of a silly hack, but a very simple solution.
                self.appState.searchTerm = self.appState.searchTerm
            }))
    }
}

struct SearchEndpointsScrollView_Previews: PreviewProvider {
    static var previews: some View {
        SearchEndpointsScrollView()
    }
}
