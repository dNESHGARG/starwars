//
//  Mocks.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/10/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

extension DirectorContainer: Mockable {
    static func mock() -> DirectorContainer {
        return DirectorContainer(searchDirector: SearchDirector.mock(),
                                 loadAllDirector: LoadAllDirector.mock(),
                                 detailsDirector: DetailsDirector.mock(),
                                 imageDirector: SwapImageDirector.mock())
    }
}

extension SearchDirector: Mockable {
    static func mock() -> SearchDirector {
        return SearchDirector(worker: SearchWorker.mock(), state: AppState())
    }
}

extension SearchWorker: Mockable {
    static func mock() -> SearchWorker {
        return SearchWorker(session: URLSession.shared)
    }
}

extension LoadAllDirector: Mockable {
    static func mock() -> LoadAllDirector {
        return LoadAllDirector(worker: LoadAllWorker.mock(), state: AppState())
    }
}

extension LoadAllWorker: Mockable {
    static func mock() -> LoadAllWorker {
        return LoadAllWorker(session: URLSession.shared)
    }
}


extension DetailsDirector: Mockable {
    static func mock() -> DetailsDirector {
        return DetailsDirector(worker: DetailsWorker.mock(), state: AppState())
    }
}

extension DetailsWorker: Mockable {
    static func mock() -> DetailsWorker {
        return DetailsWorker(session: URLSession.shared)
    }
}

extension SwapImageDirector: Mockable {
    static func mock() -> SwapImageDirector {
        return SwapImageDirector(worker: SwapImageWorker.mock(), state: AppState())
    }
}

extension SwapImageWorker: Mockable {
    static func mock() -> SwapImageWorker {
        return SwapImageWorker(session: URLSession.shared)
    }
}


extension URLSession {
    var mock: URLSession {
        return URLSession.shared
    }
}

extension SearchResponse: Mockable {
    public static func mock() -> SearchResponse {
        return SearchResponse(count: 1, next: nil, prev: nil, results: [AllDetailsModel.mock()])
    }
}

extension AllDetailsModel: Mockable {
    static func mock() -> AllDetailsModel {
        return AllDetailsModel(created: "2014-12-12T11:26:24.656000Z",
                               edited: "2014-12-15T13:07:53.386000Z",
                               url: "http://swapi.dev/api/films/2/",
                               birthYear: nil,
                               gender: nil,
                               title: "The Empire Strikes Back",
                               episodeId: 5,
                               openingCrawl: "It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed,\r\nImperial troops have driven the\r\nRebel forces from their hidden\r\nbase and pursued them across\r\nthe galaxy.\r\n\r\nEvading the dreaded Imperial\r\nStarfleet, a group of freedom\r\nfighters led by Luke Skywalker\r\nhas established a new secret\r\nbase on the remote ice world\r\nof Hoth.\r\n\r\nThe evil lord Darth Vader,\r\nobsessed with finding young\r\nSkywalker, has dispatched\r\nthousands of remote probes into\r\nthe far reaches of space....",
                               director: "Irvin Kershner",
                               producer: "Gary Kurtz, Rick McCallum",
                               releaseDate:"1980-05-17",
                               characters: ["http://swapi.dev/api/people/1/", "http://swapi.dev/api/people/2/", "http://swapi.dev/api/people/3/", "http://swapi.dev/api/people/4/", "http://swapi.dev/api/people/5/", "http://swapi.dev/api/people/10/", "http://swapi.dev/api/people/13/", "http://swapi.dev/api/people/14/", "http://swapi.dev/api/people/18/", "http://swapi.dev/api/people/20/", "http://swapi.dev/api/people/21/", "http://swapi.dev/api/people/22/", "http://swapi.dev/api/people/23/", "http://swapi.dev/api/people/24/", "http://swapi.dev/api/people/25/", "http://swapi.dev/api/people/26/"],
                               planets: ["http://swapi.dev/api/planets/4/", "http://swapi.dev/api/planets/5/", "http://swapi.dev/api/planets/6/", "http://swapi.dev/api/planets/27/"],
                               starships: ["http://swapi.dev/api/starships/3/", "http://swapi.dev/api/starships/10/", "http://swapi.dev/api/starships/11/", "http://swapi.dev/api/starships/12/", "http://swapi.dev/api/starships/15/", "http://swapi.dev/api/starships/17/", "http://swapi.dev/api/starships/21/", "http://swapi.dev/api/starships/22/", "http://swapi.dev/api/starships/23/"],
                               vehicles: ["http://swapi.dev/api/vehicles/8/", "http://swapi.dev/api/vehicles/14/", "http://swapi.dev/api/vehicles/16/", "http://swapi.dev/api/vehicles/18/", "http://swapi.dev/api/vehicles/19/", "http://swapi.dev/api/vehicles/20/"],
                               species: ["http://swapi.dev/api/species/1/", "http://swapi.dev/api/species/2/", "http://swapi.dev/api/species/3/", "http://swapi.dev/api/species/6/", "http://swapi.dev/api/species/7/"],
                               name: nil,
                               averageHeight: nil,
                               height: nil,
                               averageLifespan: nil,
                               hairColor: nil,
                               skinColor: nil,
                               eyeColor: nil,
                               designation: nil,
                               language: nil,
                               homeworld: "http://swapi.dev/api/planets/1/",
                               films: [
                                "http://swapi.dev/api/films/1/",
                                "http://swapi.dev/api/films/2/",
                                "http://swapi.dev/api/films/3/",
                                "http://swapi.dev/api/films/4/",
                                "http://swapi.dev/api/films/5/",
                                "http://swapi.dev/api/films/6/"
            ],
                               people:  [
                                "http://swapi.dev/api/people/1/",
                                "http://swapi.dev/api/people/2/",
                                "http://swapi.dev/api/people/4/",
                                "http://swapi.dev/api/people/6/",
                                "http://swapi.dev/api/people/7/",
                                "http://swapi.dev/api/people/8/",
                                "http://swapi.dev/api/people/9/",
                                "http://swapi.dev/api/people/11/",
                                "http://swapi.dev/api/people/43/",
                                "http://swapi.dev/api/people/62/"
            ],
                               model: "T-16 skyhopper",
                               manufacturer: "Corellia Mining Corporation",
                               costInCredits: nil,
                               length: nil,
                               maxAtmospheringSpeed: nil,
                               crew: nil,
                               passengers: nil,
                               cargoCapacity: nil,
                               consumables: nil,
                               hyperdriveRating: nil,
                               mglt: nil,
                               starshipClass: nil,
                               pilots:  [
                                "http://swapi.dev/api/people/1/",
                                "http://swapi.dev/api/people/2/",
                                "http://swapi.dev/api/people/4/",
                                "http://swapi.dev/api/people/6/",
                                "http://swapi.dev/api/people/7/",
                                "http://swapi.dev/api/people/8/",
                                "http://swapi.dev/api/people/9/",
                                "http://swapi.dev/api/people/11/",
                                "http://swapi.dev/api/people/43/",
                                "http://swapi.dev/api/people/62/"
            ],
                               rotationPeriod: nil,
                               orbitalPeriod: nil,
                               diameter: nil,
                               climate: nil,
                               gravity: nil,
                               terrain: nil,
                               surfaceWater: nil,
                               population: nil,
                               residents: nil,
                               mass: nil,
                               vehicleClass:nil)
    }
}
