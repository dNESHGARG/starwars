//
//  PexelImageResponse.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/18/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct ImageResults: Codable {
    let total_results: Int
    let page: Int?
    let per_page: Int?
    let photos: [Photo]?
}

struct Photo: Codable {
    let id: Int
    let width: Int?
    let height: Int?
    let url: String?
    
    let photographer: String?
    let photographer_url: String?
    let photographer_id: Int64?
    let src: [String: String]?
}

struct Source: Codable {
    let original: String?
    let large2x: String?
    let large: String?
    let medium: String?
    let landscape: String?
    let tiny: String?
    let portrait: String?
    let small: String?
}
