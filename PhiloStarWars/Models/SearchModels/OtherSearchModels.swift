//
//  SpeciesSearchModel.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/12/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

/// Not being used, but kept them for future use. Initally I had all these models
/// before I created  SearchResponse with AllDetailsModel.

// MARK: - SpeciesSearchModel

struct SpeciesSearchModel: Codable {
    let count: Int
    let next: String?
    let prev: String?
    let results: [SpeciesModel]
}

extension SpeciesSearchModel: Equatable {
    static func == (lhs: SpeciesSearchModel, rhs: SpeciesSearchModel) -> Bool {
        return lhs.count == rhs.count && lhs.results.count == rhs.results.count &&
        lhs.next == rhs.next && lhs.prev == rhs.prev
    }
}

// MARK: - FilmsSearchModel

struct FilmsSearchModel: Codable {
    let count: Int
    let next: String?
    let prev: String?
    let results: [FilmModel]
}

extension FilmsSearchModel: Equatable {
    static func == (lhs: FilmsSearchModel, rhs: FilmsSearchModel) -> Bool {
        return lhs.count == rhs.count && lhs.results.count == rhs.results.count &&
        lhs.next == rhs.next && lhs.prev == rhs.prev
    }
}

// MARK: - VehiclesSearchModel

struct VehiclesSearchModel: Codable {
    let count: Int
    let next: String?
    let prev: String?
    let results: [VehicleModel]
}

extension VehiclesSearchModel: Equatable {
    static func == (lhs: VehiclesSearchModel, rhs: VehiclesSearchModel) -> Bool {
        return lhs.count == rhs.count && lhs.results.count == rhs.results.count &&
        lhs.next == rhs.next && lhs.prev == rhs.prev
    }
}

// MARK: - PlanetsSearchModel

struct PlanetsSearchModel: Codable {
    let count: Int
    let next: String?
    let prev: String?
    let results: [PlanetModel]
}

extension PlanetsSearchModel: Equatable {
    static func == (lhs: PlanetsSearchModel, rhs: PlanetsSearchModel) -> Bool {
        return lhs.count == rhs.count && lhs.results.count == rhs.results.count &&
        lhs.next == rhs.next && lhs.prev == rhs.prev
    }
}

// MARK: - StarshipsSearchModel

struct StarshipsSearchModel: Codable {
    let count: Int
    let next: String?
    let prev: String?
    let results: [StarshipModel]
}

extension StarshipsSearchModel: Equatable {
    static func == (lhs: StarshipsSearchModel, rhs: StarshipsSearchModel) -> Bool {
        return lhs.count == rhs.count && lhs.results.count == rhs.results.count &&
            lhs.next == rhs.next && lhs.prev == rhs.prev
    }
}

// MARK: - CombinedSearchModel

struct CombinedSearchModel: Codable {
    let count: Int
    let next: String?
    let prev: String?
    let results: [AllDetailsModel]?
}

extension CombinedSearchModel: Equatable {
    static func == (lhs: CombinedSearchModel, rhs: CombinedSearchModel) -> Bool {
        return lhs.count == rhs.count
            && lhs.next == rhs.next
            && lhs.prev == rhs.prev
            && lhs.results == rhs.results
    }
}

