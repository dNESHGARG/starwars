//
//  SearchResponse.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/9/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

struct SearchResponse: Codable {
    let count: Int
    let next: String?
    let prev: String?
    let results: [AllDetailsModel]
}

extension SearchResponse: Equatable {
    static func == (lhs: SearchResponse, rhs: SearchResponse) -> Bool {
        return lhs.count == rhs.count && lhs.results.count == rhs.results.count &&
        lhs.next == rhs.next && lhs.prev == rhs.prev
    }
}
