//
//  AllDetailsModel.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/15/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

/// This is pretty heavy, but by combining all the models, we can move the
/// heavy lifting to minimal places.

struct AllDetailsModel: Codable {
    let created: String
    let edited: String
    let url: String
    
    let birthYear: String?
    let gender: String?
    let title: String?
    let episodeId: Int?
    let openingCrawl: String?
    let director: String?
    let producer: String?
    let releaseDate: String?
    let characters: [String]?
    let planets: [String]?
    let starships: [String]?
    let vehicles: [String]?
    let species: [String]?
    let name: String?
    let averageHeight: String?
    let height: String?
    let averageLifespan: String?
    let hairColor: String?
    let skinColor: String?
    let eyeColor: String?
    let designation: String?
    let language: String?
    let homeworld: String?
    let films: [String]?
    let people: [String]?
    let model: String?
    let manufacturer: String?
    let costInCredits: String?
    let length: String?
    let maxAtmospheringSpeed: String?
    let crew: String?
    let passengers: String?
    let cargoCapacity: String?
    let consumables: String?
    let hyperdriveRating: String?
    let mglt: String?
    let starshipClass: String?
    let pilots: [String]?
    let rotationPeriod: String?
    let orbitalPeriod: String?
    let diameter: String?
    let climate: String?
    let gravity: String?
    let terrain: String?
    let surfaceWater: String?
    let population: String?
    let residents: [String]?
    let mass: String?
    let vehicleClass: String?
}

extension AllDetailsModel {
    enum CodingKeys: String, CodingKey {
        
        case title,  director, producer, characters, planets, starships,
        vehicles, species, created, edited, url, name, designation, language,
        homeworld, films, people, model, manufacturer, length, crew, passengers,
        consumables, pilots, diameter, climate, gravity, terrain, population,
        residents, gender, height, mass
        
        case episodeId = "episode_id"
        case openingCrawl = "opening_crawl"
        case releaseDate = "release_date"
        case averageHeight = "average_height"
        case averageLifespan = "average_lifespan"
        case hairColor = "hair_color"
        case skinColor = "skin_color"
        case eyeColor = "eye_color"
        case costInCredits = "cost_in_credits"
        case maxAtmospheringSpeed = "max_atmosphering_speed"
        case cargoCapacity = "cargo_capacity"
        case hyperdriveRating = "hyperdrive_rating"
        case mglt = "MGLT"
        case starshipClass = "starship_class"
        case rotationPeriod = "rotation_period"
        case orbitalPeriod = "orbital_period"
        case surfaceWater = "surface_water"
        case birthYear = "birth_year"
        case vehicleClass = "vehicle_class"
    }
}

extension AllDetailsModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(created)
        hasher.combine(edited)
        hasher.combine(url)
    }
}
