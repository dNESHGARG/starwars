//
//  FilmModel.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/11/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

/// Not being used, but kept them for future use. Initally I had all these models
/// before I created a AllDetailsModel.

// MARK: - FilmModel

struct FilmModel: Codable {
    let title: String
    let episodeId: Int
    let openingCrawl: String
    let director: String
    let producer: String
    let releaseDate: String
    let characters: [String]
    let planets: [String]
    let starships: [String]
    let vehicles: [String]
    let species: [String]
    let created: String
    let edited: String
    let url: String
}

extension FilmModel {
    enum CodingKeys: String, CodingKey {
        case title
        case episodeId = "episode_id"
        case openingCrawl = "opening_crawl"
        case director
        case producer
        case releaseDate = "release_date"
        case characters
        case planets
        case starships
        case vehicles
        case species
        case created
        case edited
        case url
    }
}

extension FilmModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
        hasher.combine(episodeId)
        hasher.combine(releaseDate)
        hasher.combine(url)
    }
}

// MARK: - SpeciesModel

struct SpeciesModel: Codable {
    let name: String
    let averageHeight: String
    let averageLifespan: String
    let hairColor: String
    let skinColor: String
    let eyeColor: String
    let designation: String
    let language: String
    let homeworld: String
    let films: [String]
    let people: [String]
    let created: String
    let edited: String
    let url: String
}

extension SpeciesModel {
    enum CodingKeys: String, CodingKey {
        case name
        case averageHeight = "average_height"
        case averageLifespan = "average_lifespan"
        case hairColor = "hair_color"
        case skinColor = "skin_color"
        case eyeColor = "eye_color"
        case designation
        case language
        case homeworld
        case films
        case people
        case created
        case edited
        case url
    }
}

extension SpeciesModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(language)
        hasher.combine(designation)
        hasher.combine(url)
    }
}


// MARK: - StarshipModel

struct StarshipModel: Codable {
    let name: String
    let model: String
    let manufacturer: String
    let costInCredits: String
    let length: String
    let maxAtmospheringSpeed: String
    let crew: String
    let passengers: String
    let cargoCapacity: String
    let consumables: String
    let hyperdriveRating: String
    let mglt: String
    let starshipClass: String
    let pilots: [String]
    let films: [String]
    let created: String
    let edited: String
    let url: String
}

extension StarshipModel {
    enum CodingKeys: String, CodingKey {
        case name
        case model
        case manufacturer
        case costInCredits = "cost_in_credits"
        case length
        case maxAtmospheringSpeed = "max_atmosphering_speed"
        case crew
        case passengers
        case cargoCapacity = "cargo_capacity"
        case consumables
        case hyperdriveRating = "hyperdrive_rating"
        case mglt = "MGLT"
        case starshipClass = "starship_class"
        case pilots
        case films
        case created
        case edited
        case url
    }
}

extension StarshipModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(model)
        hasher.combine(manufacturer)
        hasher.combine(url)
        hasher.combine(passengers)
    }
}

// MARK: - PlanetModel

struct PlanetModel: Codable {
    let name: String
    let rotationPeriod: String
    let orbitalPeriod: String
    let diameter: String
    let climate: String
    let gravity: String
    let terrain: String
    let surfaceWater: String
    let population: String
    let residents: [String]
    let films: [String]
    let created: String
    let edited: String
    let url: String
}

extension PlanetModel {
    enum CodingKeys: String, CodingKey {
        case name
        case rotationPeriod = "rotation_period"
        case orbitalPeriod = "orbital_period"
        case diameter
        case climate
        case gravity
        case terrain
        case surfaceWater = "surface_water"
        case population
        case residents
        case films
        case created
        case edited
        case url
    }
}

extension PlanetModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(rotationPeriod)
        hasher.combine(orbitalPeriod)
        hasher.combine(diameter)
        hasher.combine(url)
    }
}

// MARK: - VehicleModel

struct VehicleModel: Codable {
    let name: String
    let model: String
    let manufacturer: String
    let costInCredits: String
    let length: String
    let maxAtmospheringSpeed: String
    let crew: String
    let passengers: String
    let cargoCapacity: String
    let consumables: String
    let vehicleClass: String
    let pilots: [String]
    let films: [String]
    let created: String
    let edited: String
    let url: String
}

extension VehicleModel {
    enum CodingKeys: String, CodingKey {
        case name
        case model
        case manufacturer
        case costInCredits = "cost_in_credits"
        case length
        case maxAtmospheringSpeed = "max_atmosphering_speed"
        case crew
        case passengers
        case cargoCapacity = "cargo_capacity"
        case consumables
        case vehicleClass = "vehicle_class"
        case pilots
        case films
        case created
        case edited
        case url
    }
}

extension VehicleModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(model)
        hasher.combine(manufacturer)
        hasher.combine(passengers)
        hasher.combine(url)
    }
}

// MARK: - PeopleModel

struct PeopleModel: Codable, Equatable {
    let name: String
    let height: String
    let mass: String
    let hairColor: String
    let skinColor: String
    let eyeColor: String
    let birthYear: String
    let gender: String
    let homeworld: String
    let films: [String]
    let species: [String]
    let vehicles: [String]
    let starships: [String]
    let created: String
    let edited: String
    let url: String
}

extension PeopleModel {
   enum CodingKeys: String, CodingKey {
    case name
    case height
    case mass
    case hairColor = "hair_color"
    case skinColor = "skin_color"
    case eyeColor = "eye_color"
    case birthYear = "birth_year"
    case gender
    case homeworld
    case films
    case species
    case vehicles
    case starships
    case created
    case edited
    case url
    }
}

extension PeopleModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(birthYear)
        hasher.combine(eyeColor)
        hasher.combine(hairColor)
        hasher.combine(url)
    }
}
