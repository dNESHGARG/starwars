//
//  CombinedAllResponseModel.swift
//  PhiloStarWars
//
//  Created by Dinesh Garg on 6/15/20.
//  Copyright © 2020 Dinesh Garg. All rights reserved.
//

import Foundation

/// Search response is same as loading a particular endpoint.
/// We could use SearchResponse as well instead of creating AllResponse,
/// But this is more clearer.
typealias AllResponse = SearchResponse
