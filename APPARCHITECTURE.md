APP ARCHITECTURE

Key Focus: Focus was to make app modular. It's following clean architecure principles (pretty close). Network request is generic and UI is mostly for representation only. 

Common Components:

- AppEnvironment : Bootstrapped in root, and passed along as dependencies.
    - DirectorContainer
    - AppState (EnvironmentObject)
    - Dependency Injectors
    
    Director Container: Contains directors for search, images, details etc.
  - Directors(worker: state:)
  - App State (EnvironmentObject)
  
  Worker: A protocol with default execute<> implementation
    - Service (baseurl, headers etc)
    - execution methods (search, detail, loadAll)

    
    Please refer to the architecure diagram architecure.png
    Note: It's a handdrawn file, I was doing some brainstorming for myself, and just
    sharing the same drawing as a screenhshot.
